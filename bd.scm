(define-module (bd)
  #:use-module (dbi dbi))

(define bd (dbi-open "sqlite3" "bd.sqlite"))

(define-public (créer-bd)
  (dbi-query bd (string-append "create table if not exists "
                               "téléchargements (identifiant integer primary key autoincrement,"
                               "empreinte text, date_création text, date_complétion text default "
                               "'', adresse text);"))
  (dbi-query bd (string-append
                 "create table if not exists "
                 "liste_blanche (adresse text primary key not null);")))

(define-public (enregistrer-téléchargement empreinte adresse)
  (dbi-query
   bd
   (string-append
    "insert into téléchargements (empreinte, date_création, adresse) values (\""
    empreinte "\",\"" (number->string (current-time)) "\", \"" adresse "\");")))

(define-public (supprimer-téléchargement-bd empreinte)
  (dbi-query bd (string-append "delete from téléchargements where empreinte='"
                               empreinte "';")))

(define-public (téléchargement-terminé empreinte)
  (dbi-query bd (string-append "update téléchargements set date_complétion="
                               (number->string (current-time))
                               " where empreinte='" empreinte "';")))

(define* (obtenir-liste-valeurs bd clef #:optional (accumulateur '()))
  (let ((ligne (dbi-get_row bd)))
    (if ligne
        (obtenir-liste-valeurs
         bd clef (append accumulateur `(,(assoc-ref ligne clef))))
        accumulateur)))

(define-public (obtenir-téléchargements-incomplets)
  (dbi-query bd "select empreinte from téléchargements where date_complétion='';")
  (obtenir-liste-valeurs bd "empreinte"))

(define-public (obtenir-adresse empreinte)
  (dbi-query bd (string-append
                 "select adresse from téléchargements where empreinte='"
                 empreinte "';"))
  (assoc-ref (dbi-get_row bd) "adresse"))

(define-public (obtenir-liste-blanche)
  (dbi-query bd "select adresse from liste_blanche;")
  (obtenir-liste-valeurs bd "adresse"))

(define-public (est-dans-liste-blanche? adresse)
  (dbi-query bd (string-append "select * from liste_blanche where adresse=\""
                               adresse "\";"))
  (if (dbi-get_row bd) #t #f))

(define-public (ajouter-adresse-liste-blanche adresse)
  (dbi-query bd (string-append "insert into liste_blanche (adresse) values (\""
                               adresse "\");")))

(define-public (supprimer-adresse-liste-blanche adresse)
  (dbi-query bd (string-append "delete from liste_blanche where adresse='"
                               adresse "';")))
