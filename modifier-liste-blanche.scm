#!/usr/bin/guile \
-e début -s
!#

(add-to-load-path ".")

(use-modules (bd)
             (ice-9 getopt-long))

(define (début arguments)
  (let* ((spéc-option '((aide (single-char #\a) (value #f))
                       (help (single-char #\h) (value #f))
                       (supprimer (single-char #\s) (value #t))
                       (ajouter (value #t))))
         (false-if-exception (options (getopt-long arguments spéc-option))))
    (if (or (not options)
            (option-ref options 'aide #f)
            (option-ref options 'help #f)
            (and (not (option-ref options 'supprimer #f))
                 (not (option-ref options 'ajouter #f))))
        (display "
modifier-liste-blanche [options]
-a, --aide, -h, --help    Affiche cette aide.
-s, --supprimer <adresse> Supprime une adresse de la liste blanche.
--ajouter       <adresse> Ajoute une adresse à la liste blanche.\n")
        (begin
          (let ((adresse-à-supprimer (option-ref options 'supprimer #f))
                (adresse-à-ajouter (option-ref optinos 'ajouter #f)))
            (if adresse-à-supprimer
                (supprimer-adresse-liste-blanche adresse-à-supprimer))
            (if (and adresse-à-ajouter
                     (not est-dans-liste-blanche? adresse-à-ajouter))
                (ajouter-adresse-liste-blanche adresse-à-ajouter)))))))
