#!/usr/bin/guile \
-e début -s
!#

(add-to-load-path ".")

(use-modules (ice-9 ftw)
             (ice-9 textual-ports)
             (ice-9 optargs)
             (ice-9 popen)
             (torrent)
             (bd))

;; À faire : utiliser une base de données.

(define répertoire-utilisateur (getenv "HOME"))
(define répertoire-nouveau (string-append répertoire-utilisateur "/Courrier/new"))
(define répertoire-courant (string-append répertoire-utilisateur "/Courrier/cur"))
(define téléchargements-à-surveiller '())
(define champs-en-tête '("Return-Path:" "Delivered-To:" "Received:" "From:"
                         "To:" "Subject:" "Date:" "Message-ID:" "MIME-Version:"
                         "Content-Type:" "Content-Transfer-Encoding:" ""))

(define*-public (lire-lignes #:optional (port (current-input-port))
                             (contenu ""))
  (let ((ligne (get-line port)))
    (if (not (eof-object? ligne))
        (lire-lignes port (string-append contenu "\n" ligne))
        contenu)))

(define (début arguments)
  (créer-bd)
  (set! téléchargements-à-surveiller (obtenir-téléchargements-incomplets))
  (let loop ()
    (lire-nouveaux-courriels)
    (surveiller-téléchargements)
    (sleep 10)
    (loop)))

;; Appelle la fonction traiter-courriel pour chaque courriel et
;; déplace le courriel du dossier « new » au dossier « cur ».
(define (lire-nouveaux-courriels)
  (let ((fichiers (file-system-tree répertoire-nouveau)))
    (for-each (lambda (liste)
                (let* ((fichier (open-input-file (string-append répertoire-nouveau "/" (car liste))))
                       (lignes (lire-lignes fichier)))
                  (if (est-dans-liste-blanche? (obtenir-expéditeur lignes))
                      (catch #t (lambda ()
                                  (traiter-courriel lignes))
                        (lambda (clef . arguments)
                          (if (not (eq? clef 'tentative-injection))
                              (thorw clef arguments)))))
                  (close-port fichier))
                (link (string-append répertoire-nouveau "/" (car liste))
                      (string-append répertoire-courant "/" (car liste)))
                (delete-file (string-append
                              répertoire-nouveau "/" (car liste))))
              (cddr (file-system-tree répertoire-nouveau)))))

(define (obtenir-corps courriel)
  (string-join
   (supprimer-en-tête (string-split courriel #\newline))
   ""))

(define (supprimer-en-tête liste-lignes)
  (if
   (not
    (member (car (string-split (car liste-lignes) (char-set #\tab #\space)))
            champs-en-tête))
   liste-lignes
   (supprimer-en-tête (cdr liste-lignes))))

(define* (obtenir-en-tête liste-lignes #:optional (accumulateur '()))
  (if (not
       (member (car (string-split
                     (car liste-lignes) (char-set #\tab #\space)))
               champs-en-tête))
      accumulateur
      (obtenir-en-tête (cdr liste-lignes)
                       (append accumulateur (list (car liste-lignes))))))

(define (obtenir-dernier-téléchargement ancienne-liste nouvelle-liste)
  (cond ((null? nouvelle-liste)
         #f)
         ((not (member (car nouvelle-liste) ancienne-liste))
          (car nouvelle-liste))
         (else
          (obtenir-dernier-téléchargement ancienne-liste (cdr nouvelle-liste)))))

(define (obtenir-lien corps-courriel)
  corps-courriel)

(define (envoyer-courriel destinataire sujet message)
  (define tube
    (open-output-pipe
     (string-append
      "curl -k --ssl-reqd smtp://mail.nybble.fr:587 --mail-from "
      "'robottorrent@nybble.fr' --mail-rcpt '"
      destinataire "' --upload-file - --netrc-file '/home/robottorrent/.netrc'")))
  (display "MIME-Version: 1.0\n" tube)
  (display "Content-Transfer-Encoding: 8bit\n" tube)
  (display "Content-Type: text/plain;charset=utf-8\n" tube)
  (display "From: robottorrent@nybble.fr\n" tube)
  (display (string-append "To: " destinataire "\n") tube)
  (display (string-append "Subject: " sujet "\n\n") tube)
  (display message tube)
  (close-pipe tube))

(define (obtenir-valeur-champ-en-tête en-tête champ)
  (cond ((null? en-tête)
         #f)
        ((equal? (car (string-split (car en-tête) #\:)) champ)
         (string-trim (cadr (string-split (car en-tête) #\:))
                      #\space))
        (else (obtenir-valeur-champ-en-tête (cdr en-tête) champ))))

(define (obtenir-expéditeur courriel)
  (obtenir-valeur-champ-en-tête
   (obtenir-en-tête (string-split courriel #\newline))
   "From"))

(define (envoyer-bonne-réception courriel)
  (envoyer-courriel (obtenir-expéditeur courriel) "=?UTF-8?B?Qm9ubmUgcsOpY2VwdGlvbg==?="
                    "Votre téléchargement a bien été pris en compte, vous allez recevoir
un message avec un lien permettant de récupérer vos fichiers une fois
celui-ci terminé."))

;; Lance le traitement du courriel.
;;
;; courriel est une chaine contenant le courriel.
(define (traiter-courriel courriel)
  (define liste-téléchargements (obtenir-liste-téléchargements))
  (télécharger-aimant (obtenir-lien (obtenir-corps courriel)))
  (sleep 10)
  (let ((dernier-téléchargement
         (obtenir-dernier-téléchargement
          liste-téléchargements (obtenir-liste-téléchargements))))
    (if dernier-téléchargement
        (begin
          (envoyer-bonne-réception courriel)
          (enregistrer-téléchargement dernier-téléchargement
                                      (obtenir-expéditeur courriel))
          (set! téléchargements-à-surveiller
                (obtenir-téléchargements-incomplets))))))

(define (gérer-téléchargement-terminé téléchargement)
  ;; Copier les fichiers vers le serveur HTTP.
  (let ((chemin (string-append "/home/robottorrent/torrent/" téléchargement))
        (nom (obtenir-nom téléchargement)))
    (supprimer-téléchargement téléchargement)
    (false-if-exception (mkdir chemin))
    (system (string-append "mv '/home/robottorrent/Téléchargements/"
                           nom "' " chemin))
  ;; Envoyer un courriel avec le lien.
    (envoyer-courriel (obtenir-adresse téléchargement) "=?UTF-8?B?VMOpbMOpY2hhcmdlbWVudCB0ZXJtaW7DqQ==?="
                      (string-append
                       "Votre téléchargement est terminé. Vous pouvez le récupérer à cette
adresse : http://xn--vendmiaire-e7a.fr/torrent/" téléchargement))
    (téléchargement-terminé téléchargement)
    (set! téléchargements-à-surveiller (obtenir-liste-téléchargements))))

(define (surveiller-téléchargements)
  (for-each (lambda (téléchargement)
              (write téléchargement) (newline)
              (if (téléchargement-terminé? téléchargement)
                  (gérer-téléchargement-terminé téléchargement)))
            téléchargements-à-surveiller))
