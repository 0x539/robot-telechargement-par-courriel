(define-module (torrent)
  #:use-module (ice-9 popen)
  #:use-module (ice-9 rdelim)
  #:use-module (ice-9 regex)
  #:use-module (ice-9 match)
  #:use-module (sxml match)
  #:use-module (sxml simple))

(setenv "XMLRPC_TRACE_XML" "1")

(define-public (obtenir-liste-téléchargements)
  (let ((retour (obtenir-tableau (lancer-commande "download_list"))))
    (if (not retour) '() retour)))

(define-public (obtenir-nom empreinte)
  (cadr
   (obtenir-valeur-paramètre
    (lancer-commande (string-append "d.name " empreinte)) 0)))

(define-public (télécharger-aimant lien)
  (if (string-match "'" lien)
      (throw 'tentative-injection
             (string-append "Il y a eu une tentative d’injection de code par"
                            " le lien aimant d’un courriel.")))
  (lancer-commande (string-append "load_start '" lien "'")))

(define-public (supprimer-téléchargement empreinte)
  (lancer-commande (string-append "d.erase " empreinte)))

(define-public (téléchargement-terminé? empreinte)
  (string=? "1"
            (cadr (obtenir-valeur-paramètre
                   (lancer-commande
                    (string-append "d.complete " empreinte)) 0))))

(define (lire-jusqu-à-fin-fichier port)
  (let ((ligne (read-line port 'concat)))
    (if (eof-object? ligne)
        ""
        (string-append ligne (lire-jusqu-à-fin-fichier port)))))

(define (supprimer-premier-motif-chaîne motif chaîne)
  (let ((résultat (string-match motif chaîne)))
    (if résultat
        (begin
          (string-append (match:prefix résultat)
                         (match:suffix résultat)))
        #f)))

(define* (supprimer-complètement-motif-chaîne motif chaîne #:optional
                                              (sauvegarde chaîne))
  (let ((résultat (supprimer-premier-motif-chaîne motif chaîne)))
    (if (not résultat)
        sauvegarde
        (supprimer-complètement-motif-chaîne motif résultat))))

(define (obtenir-réponse chaîne)
  (let* ((motif "XML-RPC RESPONSE:\n\n")
         (résultat (string-match motif chaîne)))
    (xml->sxml
     (supprimer-complètement-motif-chaîne "\\\\r\\\\n" (match:suffix résultat)))))

(define (lancer-commande commande)
  (let ((port (tmpfile)))
    (with-error-to-port port (lambda ()
                               (let ((sortie (open-input-pipe
                                              (string-append "xmlrpc localhost " commande))))
                                 (close-pipe sortie))))
    (seek port 0 SEEK_SET)
    (obtenir-réponse (lire-jusqu-à-fin-fichier port))))

(define (obtenir-réponse-xml document)
  (car
   (delete "\n"
           (sxml-match document [(*TOP* (*PI* . ,q)
                                        (methodResponse . ,a)) a]
                       [,otherwise #f]))))

(define (obtenir-paramètres document)
  (define réponse (obtenir-réponse-xml document))
  (delete "\n"
          (sxml-match réponse
                      [(params . ,a) a]
                      [,otherwise #f])))

(define (obtenir-valeur-paramètre document n)
  (define paramètres (obtenir-paramètres document))
  (sxml-match (list-ref paramètres n)
              [(param (value ,a)) a]
              [,otherwise #f]))

(define* (obtenir-valeurs paramètres #:optional (accumulateur '()))
  (cond ((null? paramètres) accumulateur)
        (else (obtenir-valeurs (cdr paramètres)
                               (append
                                accumulateur
                                (list (sxml-match (car paramètres)
                                                  [(value (string ,a)) a]
                                                  [,otherwise #f])))))))

(define (obtenir-tableau document)
  (define paramètres (obtenir-paramètres document))
  (obtenir-valeurs
   (delete
   "\n"
   (sxml-match (car paramètres)
               [(param (value (array (data ,a ...)))) (list a ...)]
               [,otherwise #f]))))
